var section = $('.navigator li#s0');
function initialize(){
	nav = $('body').innerHeight()*0.88;
	$('.header').css({'height': nav});
	$('.header').css({'margin-top': $('.navigator').height()});
	$('.header').css({'width': nav*2.26});
	$('.header').css({'margin-left': ($('body').width() - $('.header').width())/2});
	$('.solutions').css({'height': nav-$('body').innerHeight()*0.04});
	$('.solutions').css({'width': nav*2.26});
	$('.solutions').css({'margin-left': ($('body').width() - $('.solutions').width())/2});
	$('.solutions .content .solution div').css({'width': $('.solutions .content .solution div').height()});
	$('.edugo').css({'height': nav});
	$('.edugo').css({'width': nav*2.26});
	$('.edugo').css({'margin-left': ($('body').width() - $('.edugo').width())/2});
	
	$('.projects').css({'height': nav});
	$('.projects').css({'width': nav*2.26});
	$('.projects').css({'margin-left': ($('body').width() - $('.projects').width())/2});
	$('.project').css({'width': $('.project').height()});

	$('.partners').css({'height': nav});
	$('.partners').css({'width': nav*2.26});
	$('.partners').css({'margin-left': ($('body').width() - $('.partners').width())/2});

	$('.team').css({'height': '92%'});
	$('.team').css({'width': nav*2.26});
	$('.team').css({'margin-left': ($('body').width() - $('.team').width())/2});

	$('.footer').css({'height': '92%'});
	$('.footer').css({'width': nav*2.26});
	$('.footer').css({'margin-left': ($('body').width() - $('.footer').width())/2});
	$('.footer .icon').css({'width': $('.footer .icon').height()});

	$('.team .tool').css({'height': $('.team .tool').width()});

	$('.jtextfill').textfill({ });

	$('.navigator li span').css({
		'font-size': $('.navigator li#s6 span').css('font-size'),
	});

	$('.navigator li').animate({'line-height': ($('.navigator li#s6').height())+'px',},100);

	$('.solutions solution p').css({
		'font-size': $('.solutions div#3').parent().find('span').css('font-size'),
		'line-height': $('.solutions div#3').parent().find('span').css('line-height'),
	});

	$('.active').css({'width': section.width()});
	$('.active').css({'left':section.offset().left});

	$('.node').css({'width': $('.node').height()});
	$('.node').css({'margin-top': ($('.nodes').height() - $('.node').height())/2});

}
var main = function(){
	initialize();
	$('.about').fadeTo(1000,1);
	$('.nodes').fadeTo(1000,1);
	$('.team .members p').fadeTo(1,0);
	$(window).bind("scroll",function(){
		if(scrolltime) return;
		scrolltime=true;
		down = ($(window).scrollTop()>scrolled);
		scrolled = $(window).scrollTop();
		scrollCtrl();
	});

	$(window).resize(function(){
		//$('.header p').css('font-size',$(window).width()*0.005 + 14.255);	//f = 0.005w + 20.255
		initialize();
		$(window).scrollTop(parseInt(section[0].id[1],10)*nav);
	});

	$(window).on('beforeunload', function(){
	  $(window).scrollTop(0);
	});
	
	for(var i=1;i<9;i++){
		$('.solutions div#'+i).css('background-position-x', 100*(1-i)+'%');
	}

		$('.navigator li').css('line-height', ($('.navigator ul').height())+'px');

	$('.navigator li').bind('click',navClick);

	$('.team .tool').hover(function(){
		$(this).parent().children("p").animate({'top':-120, 'opacity':1},300);
	},function(){
		$(this).parent().children("p").animate({'top':30, 'opacity':0},300);
	});

}

$(document).ready(main);