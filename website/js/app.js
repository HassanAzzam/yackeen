	var app = angular.module('Viewer',[]);
	app.directive('project',function(){
		return {
			restrict: 'E',
			templateUrl: './project.html'
		};
	});
	app.controller('projectsctrl',['$scope','$http', function($scope,$http){
		var desc="a";
		var tmp = this;
		tmp.projects=[];
		
		$http.get("assets/Projects.json").success(function(data){
			tmp.projects = data; 
		}).error(function(data){ console.log(data); });

		$scope.enableFlip = function(){
					$('project').css({'width': $('project').height()});
					$('.jtextfill').textfill({ });
                    $('project').flip({ toggle : 'hover'});
					$('project').hover(function(){ $(this).css("z-index",1); $(this).flip(true); },function(){ $(this).flip(false,function(){$(this).css("z-index",0);}); });
        };
	}]);

	app.directive("repeatEnd", function(){
            return {
                restrict: "A",
                link: function (scope, element, attrs) {
                    if (scope.$last) {
                        scope.$eval(attrs.repeatEnd);
                    }
                }
            };
        });
