var duration = 1000;
var scrolltime = false;

function fadeAll(exclude){
	if(exclude!=0)$('.about').fadeTo(duration,0),$('.nodes').fadeTo(duration,0);
	if(exclude!=1)$('.solutions').children().fadeTo(duration,0);
	if(exclude!=2)$('.edugo .content').fadeTo(duration,0);
	if(exclude!=3)$('.projects').children().fadeTo(duration,0);
	if(exclude!=4)$('.partners').children().fadeTo(duration,0);
	if(exclude!=5)$('.team').children().fadeTo(duration,0);
}

function animateScroll(pos){
	$('.header img').animate({'top':-pos/2},duration);
	$('.edugo img').animate({'top':nav*2-pos*1.055},duration);
	$('body').animate({'scrollTop':pos},duration,function(){
		setTimeout(function(){scrolltime=false;},500);
	});
}

var down = false;
var scrolled = 0;
var nav;
function scrollCtrl(){

	var bodyHeight = $('body').innerHeight();

	if((scrolled<=5*nav&&scrolled%nav==0)||scrolled==5*nav+bodyHeight+bodyHeight*0.08) return;

	if(!down) {
		scrolled=nav*Math.floor(scrolled/nav);
		if(scrolled==6*nav) scrolled=5*nav;
	}
	else {
		scrolled=nav*Math.ceil(scrolled/nav);
		if(scrolled>=6*nav) scrolled=5*nav+bodyHeight+bodyHeight*0.08;
	}

	section = $('.navigator li#s'+Math.floor(scrolled/nav));
	$('.active').animate({'left':section.offset().left},duration);

	if(!scrolled){
		$('.navigator li').animate({'line-height': (bodyHeight*0.12)+'px',},duration);
		$('.navigator').animate({'height':'12%'},duration);

		if($('.logo').width()==35){
			$('.logo').animate({'width':150},duration/2,function(){
				$('.logo').css({'background-size': 'contain'});
				$('.logo').animate({'width':250},duration/2);
			});
			
		}
	}
	else{
		$('.navigator li').animate({'line-height': (bodyHeight*0.08)+'px',},duration);
		$('.navigator').animate({'height':'8%'},duration);
		if($('.logo').width()==250){
			$('.logo').animate({'width':150},duration/2,function(){
				$('.logo').css({'background-size': '150px'});
				$('.logo').animate({'width':35},duration/2);
			});
		}
		
	}
	
	fadeAll(scrolled/nav);
	if(scrolled==0)	$('.about').fadeTo(duration,1),$('.nodes').fadeTo(duration,1);
	else if(scrolled==nav) $('.solutions').children().fadeTo(duration,1);
	else if(scrolled==2*nav) $('.edugo .content').fadeTo(duration,1);
	else if(scrolled==3*nav) $('.projects').children().fadeTo(duration,1);
	else if(scrolled==4*nav) $('.partners').children().fadeTo(duration,1);
	else if(scrolled==5*nav) $('.team').children().fadeTo(duration,1);
	animateScroll(scrolled);
}

function navClick(){
	if(scrolltime) return;
	var sc = parseInt($(this)[0].id[1],10)*nav;
		if(sc==6*nav) sc =5*nav + $('body').innerHeight()*1.08;
		if(sc==scrolled) return;
		scrolltime=true;
		down = (sc>scrolled);
		if(down) scrolled = sc-1;
		else scrolled = sc + 1;
		scrollCtrl();
}