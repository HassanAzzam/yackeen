var landscape;
var section = $('.navigator li#s0');

function centerWidth(el){
	el.each(function(){
		$(this).css('margin-left',($(this).parent()[0].getBoundingClientRect().width-$(this)[0].getBoundingClientRect().width)/2);
	});
}

function centerHeight(el){
	el.each(function(){
		$(this).css('margin-top',($(this).parent()[0].getBoundingClientRect().height-$(this)[0].getBoundingClientRect().height)/2);
	});
}

function enableFlip(){
	var el = $('project .card');
	el.flip({ toggle:'manual' });
	if(landscape) el.hover(function(){ 
		$(this).css("z-index",1); $(this).flip(true,function(){$(this).css("z-index",0);});
	},function(){ 
		$(this).flip(false,function(){$(this).css("z-index",0);});
	});
	else el.on('touch',function(){  
		$(this).css("z-index",1); $(this).flip(true,function(){$(this).css("z-index",0);}); 
	});
}

function square(el,str){
	if(str==='width') el.css({'width': el[0].getBoundingClientRect().height});
	else el.css({'height': el[0].getBoundingClientRect().width});
}

function solutionsAlign(){
		var el = $('.solution div');
		square(el,'width');
		centerWidth(el);
		for(var i=1;i<9;i++){
			$('.solutions div#'+i).css('background-position-x', el[0].getBoundingClientRect().width*(1-i));
		}
	}

var timeout = [null,null,null];
	function unhover(loc){
		var ind = loc[0].id[3];
		if(timeout[parseInt(ind,10)-1]===null) return;
			var el = $('.logos li:nth-child('+ind+')');
			var linkwidth = el.find('a').width();
			var curwidth = el.height()*0.5;
			el.find('a').animate({'height':'50%'},150);
			el.find('.popup').animate({'top':'-30%', 'opacity':'0'},150,function(){
				timeout[parseInt(ind,10)-1] = null;
			});
			el.find('a img').animate({
				'margin-left': (linkwidth - curwidth)/2,
				'width': curwidth
			},150);
	}

	function partnersMain(){
		$('.map').css({
			'height':'80%',
			'width':'100%'
		});
		if(landscape){
			$('.map').css('width',$('.map').height()*1.8);
			square($('.logos a img'),'width');
		}
		else{
			$('.map').css('height',$('.map').width()/1.8);
			square($('.logos a img'),'width');
			$('.logos').css('width','95%');
		}
		
		$('.location').css({'height': $('.location').width()*1.333});
		$('.location').hover(function(){
			var ind = $(this)[0].id[3];
			if(timeout[parseInt(ind,10)-1]!=null) return;
			var el = $('.logos li:nth-child('+ind+')');
			var linkwidth = el.find('a').width();
			var curwidth = el.height()*0.65;
			el.find('a').animate({'height':'65%'},150);
			el.find('.popup').animate({'top':'5%', 'opacity':'1'},150);
			el.find('a img').animate({
				'margin-left': (linkwidth - curwidth)/2,
				'width': curwidth
			},150);

		},function(){
			var ind = $(this)[0].id[3];
			clearTimeout(timeout[parseInt(ind,10)-1]);
			var tmp = $(this);
			timeout[parseInt(ind,10)-1] = setTimeout(function() { unhover(tmp)}, 150);
		});
	}

function projectsAlign(){
		var el = $('.projects .content .obj'); 
		if(landscape)
		el.css({
			'width':'33.333%',
			'height': '50%'
		});
	else
		el.css({
			'width':'50%',
			'height': '33.333%'
		});
		el.css({'height':(Math.min(el[0].getBoundingClientRect().height,el[0].getBoundingClientRect().width))});
		el.css({'width':(Math.min(el[0].getBoundingClientRect().height,el[0].getBoundingClientRect().width))});
		el = $('.projects .content');
		centerHeight(el);
		el.css('margin-top',parseFloat(el.css('margin-top'),10)-$('body').innerHeight()*0.04);
	}
function team(){
		//itemsLayout($('.members'),$('.member'),4,2);
		centerWidth($('.team img'));
		$('team h6 span').css({'font-size': $('team h6.jtextfill span').css('font-size')});
		$('team .pic span').css({'font-size': $('team .pic .jtextfill span').css('font-size')});
		$('.team .member .pic').hover(function(){
			$(this).find('img').animate({'opacity':0.5},300);
			$(this).find('div').animate({'opacity':1},300);
		},function(){
			$(this).find('img').animate({'opacity':1},300);
			$(this).find('div').animate({'opacity':0},300);
		});
}


function initialize(){
	landscape = ($('body').innerWidth()>=$('body').innerHeight());
	setTimeout(function(){
		enableFlip();
		projectsAlign();
		partnersMain();
		solutionsAlign();
		team();
		centerWidth($('.center-width'));
		centerHeight($('.center-height'));
	},3000);
	nav = $('body').innerHeight();
	$('.jtextfill').textfill({});
	if(section.is($('.navigator li#s0'))) $('.navigator').css('height', '12vh');
	else $('.navigator').css('height', '8vh');
	square($('.footer .icon'),'height');
	$('.active').css({'width': section[0].getBoundingClientRect().width});
	$('.active').css({'left':section.offset().left});
	$('.navigator li span').css('font-size', $('#s6 span').css('font-size'));
	$('.navigator li').css({'line-height': ($('.navigator').height())+'px'});	
	
}

function mail(){
	window.open("mailto:support@yackeensolutions.com");
}
var main = function(){
	initialize();
	setTimeout(function(){
		$('.mask, .splashbg').remove();
		$('.navigator').css({'position':'fixed', 'z-index':999});
		if(landscape) $('.logo').animate({'width':'30vh'},300);
		$('body').css({'overflow':'auto'});
	},5000);
	$('body').on({
	    'mousewheel': function(e) {
	        if (!scrolltime) return;
	        e.preventDefault();
	        e.stopPropagation();
	    },
	    'touchmove': function(e) {
	        if (!scrolltime) return;
	        e.preventDefault();
	        e.stopPropagation();
	    }
	});

	$(window).bind("scroll",function(){
		if(scrolltime||Math.floor(scrolled)==$(window).scrollTop()) return;
		scrolltime=true;
		down = ($(window).scrollTop()>scrolled);
		scrolled = $(window).scrollTop();
		scrollCtrl();
	});
	

	$(window).resize(function(){
		$(window).scrollTop(parseInt(section[0].id[1],10)*nav);
		initialize();
		$('.logo').css({'top':$('.navigator').height()/2-$('.logo').height()/2});
	});

	$(window).on('beforeunload', function(){
	  $(window).scrollTop(0);
	});

	$('.navigator li').bind('click',navClick);
	$('.logo').bind('click',navClick);
	$('.toTop').bind('click',navClick);
}

$(window).ready(main);