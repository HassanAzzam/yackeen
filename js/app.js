	var app = angular.module('Viewer',[]);
	app.directive('project',function(){
		return {
			restrict: 'E',
			templateUrl: './project.html'
		};
	});
	app.controller('projectsctrl',['$scope','$http', function($scope,$http){
		var desc="a";
		var tmp = this;
		tmp.projects=[];
		
		$http.get("assets/Projects.json").success(function(data){
			tmp.projects = data; 
		}).error(function(data){ console.log(data); });
	}]);

